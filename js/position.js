/*
Position represents a position in the editor. A position consists of a row and a column.
Row and column are both zero-based, so row 0 is the row with line number 1.

A "global" position is in fact a character index. If the first row contains 30 characters,
and a second row contains also 30 characters, then the fifteenth character in the second row
has global index 30+15=45. This global index is used in the communication with the backend.
The backend has no concept of rows, just of tokens.
*/
function Position(row, column)
{
    this.row = row;
    this.column = column;

    this.smallerEqual = function(other)
    {
      return other.row > this.row || (other.row == this.row && other.column >= this.column);
    }

    this.greaterEqual = function(other)
    {
      return !this.smallerThan(other);
    }

    this.smallerThan = function(other)
    {
      return other.row > this.row || (other.row == this.row && other.column > this.column);
    }

    this.greater = function(other)
    {
      return !this.smallerEqual(other);
    }

    this.toGlobalPosition = function()
    {
        var totalLength = 0;
        for(var i = 0; i < this.row; i++)
            totalLength += editor.getSession().getLine(i).length;

        return totalLength + column;
    }

    this.isZero = function()
    {
      return (row == 0 && column == 0);
    }
}

Position.ZERO = new Position(0, 0);

// Used to convert a global position back to a Position with row and column.
Position.fromIndex = function(/* Integer */ index) {
  var row = 0;
  var column = 0;

  var remaining = index;
  while (remaining > 0 && row < editor.getSession().getLength()) {
    var lineLength = editor.getSession().getLine(row).length;
    if (lineLength >= remaining) {
      column = remaining;
      break;
    }

    remaining -= lineLength;
    row++;
  }

  return new Position(row, column);
}
