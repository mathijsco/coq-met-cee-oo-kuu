/**
 * The prover handles high level communication with the server, using
 * the ajax class. It has an internal queue and processes the items in
 * this queue by sending them to the server. It takes care of listen
 * requests, display requests, and formats the data received by the
 * server. All responses are propagated via changeable callbacks.
 */
function Prover() {

  var DEFAULT_CALLBACK = function() { console.log(arguments); };

  // Token the server uses
  var TOKEN = "__PWT__";

  // The send queue and the index in the queue
  var queue = new Array();
  var queueIndex = 0;

  // The function that should be called when a tactic is proved or undone
  var proveCallback = DEFAULT_CALLBACK;

  // The default display type, and the function that should be called whenever the server returns a display response
  var displayType = Prover.Display.NONE;
  var displayCallback = DEFAULT_CALLBACK; // Default to log

  // A flag to indicate whether a 'listen' is in progress
  var listening = false;

  // Public functions

  this.setProveCallback = setProveCallback;
  function setProveCallback(/* function(Integer, String, String) */ callback) {
    proveCallback = callback;
  }

  this.setDisplay = setDisplay;
  function setDisplay(/* Prover.Display */ type) {
    if (type !== Prover.Display.NONE && type !== Prover.Display.NATURAL && type !== Prover.Display.GENTZEN && type !== Prover.Display.FITCH) {
      console.error("Display type '" + type + "' is not valid");
      return;
    }

    displayType = type;
  }

  this.setDisplayCallback = setDisplayCallback;
  function setDisplayCallback(/* function(String) */ callback) {
    displayCallback = callback || DEFAULT_CALLBACK;
  }

  /**
   * Adds a display call with the currently set display type to the queue. The set
   * display callback function is invoked upon completion of the request.
   */
  this.display = display;
  function display(/* Integer */ toIndex) {
    if (queue.length !== 0)
      return;

    queue.push(new Tactic(Tactic.Type.DISPLAY, displayType, toIndex, toIndex));
    _sendQueue(true);
  }

  /**
   * Adds the specified array of tactics to the queue.
   */
  this.prove = prove;
  function prove(/* Tactic[] */ tactics) {
    Progress.setMinimum(tactics[0].startIndex);
    Progress.setMaximum(tactics[tactics.length - 1].endIndex);
    _pushTactics(tactics);
  }

  /**
   * Adds a undo action to the queue. The index specified is the index in the currently
   * proved text.
   */
  this.undo = undo;
  function undo(/* Integer */ toIndex, /* Integer */ fromIndex) {
    Progress.setUndo(true);
    Progress.setMinimum(toIndex);
    Progress.setMaximum(fromIndex);
    var tactics = [new Tactic(Tactic.Type.UNDO, null, toIndex, null)];

    _pushTactics(tactics);
  }

  // Private functions

  /**
   * Pushes the specified array of tactics to the queue. If the queue was empty prior
   * to pushing these tactics, the first tactic is sent immediately.
   */
  function _pushTactics(/* Tactic[] */ tactics) {
    queue = queue.concat(tactics);

    if (queue.length === tactics.length) { // If the queue was empty
      _sendQueue(false);
    }
  }

  /**
   * Sends the items in the queue to the server. Whenever possible, the tactics
   * are combined in a single request, so that we don't send 30 request if a
   * single one will do. Two tactics can be combined into one if they either are
   * both of type PROVE, or both of type UNDO. From all combinable tactics, one
   * request string is built, with each tactic separated by the server token and
   * the tactics start and end index.
   */
  function _sendQueue(/* Boolean */ displayRequest) {
    if (queue.length === 0)
      return;
      
    // If the last sent tactic failed, the user may have already queued the next tactic
    // (this is possible because the client does not know if a tactic will fail beforehand)
    // In this case, i.e. the index we proved is smaller than the requested tactic, the queue
    // should be emptied (as all entries after the next one can only be tactics after the
    // failed one) and the callback should be invoked with no values, except 'completed'.
    // The interface can now be updated and no illegal tactics have been sent.
    if (queue[0].type === Tactic.Type.PROVE && provedIndex < queue[0].startIndex) {
      queue = [];
      proveCallback(null, null, null, null, true);
      return;
    }

    var command = null;
    var args = queue[0].startIndex;
    var tactics = new Array();

    // Group all calls of the same type
    for (queueIndex = 0; queueIndex < queue.length; ++queueIndex) {
      var tactic = queue[queueIndex];

      command = command || tactic.type;
      if (command !== tactic.type)
        break;

      if (command === Tactic.Type.PROVE) {
        args += TOKEN + tactic.text + TOKEN + tactic.endIndex;
      }
      else if (command === Tactic.Type.UNDO) {
        args = tactic.startIndex;
      }

      tactics.push(tactic);

      // We don't want to add proving rules to our display request
      if (displayRequest) {
        queueIndex++; // We're breaking, so we need to increment our index manually
        break;
      }
    }

    // Post the command
    ajax.post(command, args, _processResult(tactics, displayRequest));
  }

  /**
   * Creates a callback function for the specified tactics.
   * The resulting function takes some server response ('response'), and handles it. If
   * no data is returned, the server is not done yet and a listen tactic is prepended
   * to the queue. If we did receive data, there are two options. Either the tactic
   * we sent was a request for some display update (e.g. a proof tree), in which case
   * the display callback function should be invoked with the tree data. In the other
   * case, and if we were currently listening, we check if the server has completed
   * listening (++ is returned). If the server did not finish, we send another listen.
   * In all other cases it concerned a prove or undo call. From the data we extract the
   * server's messages, determine the relevant indexes (from what index to what index
   * did we just prove or undo), and send a prove callback. After this, we prepend a
   * listen call to the queue, because the server may not be done with the sent tactics
   * yet. Finally, we send a display call (if appropriate).
   * In all cases, we end by sending the next item in the queue.
   */
  var lastTactic = {};
  var provedIndex = 0;
  function _processResult(/* Tactic[] */ sentTactics, /* Boolean */ displayRequest) {
    return function(/* String */ response) {
      var requestDisplay = false;

      queue = queue.slice(queueIndex); // We got a response; remove the tactics we sent

      // If the response is empty, the server is still busy; we should start listening.
      // We stop listening as soon as the server responds with "++" on a listen request.
      if (response.length === 0) {
        queue.unshift(new Tactic(Tactic.Type.LISTEN));
        listening = true;
        requestDisplay = displayRequest;
      }
      else if (!displayRequest) {

        // If we are still listening for server responses, we should add a new listen
        // request to the beginning of the queue; the server may still have data for us.
        if (listening) {
          if(response == "++") {
            listening = false;
            proveCallback(null, null, null, null, queue.length === 0);

            _sendQueue(requestDisplay);
            return;
          }

          queue.unshift(new Tactic(Tactic.Type.LISTEN));
        }

        response = response.replace(/\+/g, "");

        var messages = _extractMessages(response);
        var warnings = _extractWarnings(response);

        var provedRange = _extractProvedRange(sentTactics, response);
        var provedFrom = provedRange.provedFrom;
        var provedTo = provedRange.provedTo;

        provedIndex = provedTo;

        proveCallback(provedFrom, provedTo, messages, warnings);

        // If provedTo is not the end index of last tactic, we should add a new listen
        // request to the beginning of the queue; the server still has data for us.
        if (provedTo < sentTactics[sentTactics.length - 1].endIndex) {
          queue.unshift(new Tactic(Tactic.Type.LISTEN));
          listening = true;
        }

        // If we should request a new display tree, prepend a call for this
        if (provedTo > 0 && displayType !== Prover.Display.NONE && queue) {
          prover.display(provedTo);
        }
      }
      else {
        var display = _extractDisplay(response);

        displayCallback(display);
      }

      _sendQueue(requestDisplay);
    };
  }

  /**
   * Extracts the display part of the server's response, and returns it.
   */
  function _extractDisplay(/* String */ response) {
    var displayStartIndex = response.indexOf("+") + "+".length;
    var displayEndIndex = response.indexOf(TOKEN);
    return response.substring(displayStartIndex, displayEndIndex);
  }

  /**
   * Extracts the messages part of the server's response, and returns it.
   */
  function _extractMessages(/* String */ response) {
    var messagesEndIndex = response.indexOf(TOKEN);
    var messages = response.substring(0, messagesEndIndex);
    return (messages.length > 0 ? messages.split("\n") : []);
  }

  /**
   * Extracts the warnings part of the server's response, and returns it.
   */
  function _extractWarnings(/* String */ response) {
    var warningsStartIndex = response.lastIndexOf(TOKEN) + TOKEN.length;
    var warnings = response.substring(warningsStartIndex).replace(/Warning: /g, "");
    return (warnings.length > 0 ? warnings.split("\n") : []);
  }

  /**
   * Extracts the proved range out of the server's response, and returns it. The
   * end of this range is determined by the server, and the start of this range
   * is the start index of the first tactic sent, or, if no tactic was sent (i.e.
   * a listen request was sent), the last sent tactic's start index is the 'from'.
   * An object {provedFrom: <Integer>, provedTo: <Integer>} is returned.
   */
  function _extractProvedRange(/* Tactic[] */ sentTactics, /* String */ response) {
    var provedToStartIndex = response.indexOf(TOKEN) + TOKEN.length;
    var provedToEndIndex = response.lastIndexOf(TOKEN);
    var provedTo = parseInt(response.substring(provedToStartIndex, provedToEndIndex));

    var provedFrom = sentTactics[0].startIndex;
    if (provedFrom === undefined)
      provedFrom = lastTactic.startIndex;
    else
      lastTactic = sentTactics[0];

    return {provedTo: provedTo, provedFrom: provedFrom};
  }
}

Prover.Display = {
  NONE: "None",
  NATURAL: "Dump Natural Deduction",
  GENTZEN: "Dump Gentzen Deduction",
  FITCH: "Dump Fitch Deduction"
};
