define("ace/mode/coq_highlight_rules", [
  "require",
  "exports",
  "module",
  "ace/lib/oop",
  "ace/mode/text_highlight_rules"
], function(require, exports, module){
"use strict";

  var oop = require("../lib/oop");
  var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;
  var CoqHightlightRules = function() {

    var keywordMapperCase = this.createKeywordMapper({
      keyword: "Require|Import|Theorem|Definition|Variable|Variables|Axiom|"
             + "Parameter|Parameters|Hypothesis|Hypotheses|Conjecture|Proof|"
             + "Inductive|CoInductive|Fixpoint|CoFixpoint|Lemma|Remark|Fact|"
             + "Corollary|Proposition|Example|Check|Defined|Admitted|Claim|Qed",
      identifier: "IF|Prop|Set|Type"
    }, "name", true);

    var keywordMapper = this.createKeywordMapper({
      identifier: "_|as|at|cofix|else|end|exists|exists2|fix|for|forall|fun|if"
                + "in|let|match|mod|return|then|using|where|with"
    }, "other", true);

    this.$rules = {
        "start" : [{
          token : "comment",
          start : "\\(\\*",
          end : "\\*\\)"
        }, {
            token : "numeric",
            regex : "[+-]?\\d+((\\.\\d*)?([eE][+-]?\\d+)?)?\\b"
        }, {
            token : "proofweb",
            regex : "((f_)?con_i|(f_)?con_e1|(f_)?con_e2|(f_)?dis_i1|(f_)?dis_i2|"
              + "(f_)?dis_e|(f_)?imp_i|(f_)?imp_e|(f_)?neg_i|(f_)?neg_e|(f_)?fls_e|"
              + "(f_)?tru_i|(f_)?negneg_i|(f_)?negneg_e|(f_)?LEM|(f_)?PBC|(f_)?MT|"
              + "(f_)?all_i|(f_)?all_e|(f_)?exi_i|(f_)?exi_e|(f_)?equ_i|(f_)?equ_e|"
              + "(f_)?equ_e'|exact|insert|unfold|foreach|replace|lin_solve|assumption)\\b"
        }, {
            token : keywordMapperCase,
            regex : "[A-Z][a-zA-Z0-9'_]*\\b"
        }, {
            token : keywordMapper,
            regex : "[a-z_][a-zA-Z0-9_']*\\b"
        }, {
            token : "operator",
            regex : "\\*|\\+|\\+|\\-|\\-|>|<|\\(|\\)|\\?|\\;|\\\\|\\/|\\]|\\[|\\||\\{|\\}|\\~|\\:|=_D|\\!|\\%|\\&|\\@|\\^|\\="
        }, {
          token : "dot",
          regex : "\\."
        }, {
            token : "text",
            regex : "\\s+"
        }]
    };
    this.normalizeRules();
  };
  oop.inherits(CoqHightlightRules, TextHighlightRules);

  exports.CoqHighlightRules = CoqHightlightRules;
});

define("ace/mode/coq",[
  "require",
  "exports",
  "module",
  "ace/lib/oop",
  "ace/mode/text",
  "ace/mode/coq_highlight_rules",
  "ace/range"
], function(require, exports, module) {
  "use strict";

  var oop = require("../lib/oop");
  var TextMode = require("./text").Mode;
  var CoqHighlightRules = require("./coq_highlight_rules").CoqHighlightRules;

  var Mode = function() {
    this.HighlightRules = CoqHighlightRules;
  };

  oop.inherits(Mode, TextMode);

  (function() {
      this.blockComment = {start: "--", end: "--"};
      this.$id = "ace/mode/coq";
  }).call(Mode.prototype);

  exports.Mode = Mode;
});
