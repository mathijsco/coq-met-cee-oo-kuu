/*
Index.js contains the basic setup for the whole proofweb-editor.
Here, actions are attached to buttons, callbacks are set, and initialization is handled.
*/
var Range = ace.require('ace/range').Range;

var prover = new Prover();
var proved = new Position(0, 0);
var proving = new Position(0, 0);
var changed = false;

/**
 * This callback is called after a response to a prove or undo request was received.
 * The proving/proved positions are updated, just like the makers and the button
 * states. Furthermore, the messages and warnings are displayed on the right bar.
 */
prover.setProveCallback(function(provedFrom, provedTo, messages, warnings, completed) {
  if (completed !== undefined) {
    if (completed) {
      Progress.done(proved.toGlobalPosition());
      proving = new Position(proved.row, proved.column);
      updateMarkers(proved, proving);
      determineActionButtonStates();
    }
    return;
  }

  Progress.setCurrent(provedTo);
  proved = Position.fromIndex(provedTo);
  if (provedFrom >= provedTo || proved.greaterEqual(proving)) {
    proving = new Position(proved.row, proved.column);
  }

  updateMarkers(proved, proving);
  determineActionButtonStates();

  var updateDisplay = function(list, elem) {
    elem.html(replaceUnicode(list.join("<br>")));
    if (list.length === 0)
      elem.addClass("hidden");
    else
      elem.removeClass("hidden");
  };

  if(provedFrom > provedTo || (provedFrom == provedTo && provedTo == 0) || (messages && messages.length > 0))
    updateDisplay(messages, $("#coq-result"));
  updateDisplay(warnings, $("#coq-warnings"));
});

/**
 * The display callback is called when a new tree was received from the server.
 * The tree data is formatted (to remove unicode stuff) and displayed on the
 * right bar.
 */
prover.setDisplayCallback(function(tree) {
  $("#coq-tree .content").html(replaceUnicode(tree));
  $("#coq-tree .content br").remove();
});

/**
 * Converts the string's unicodes (\uXXXX) to characters.
 */
function replaceUnicode(/* String */ unicodeString) {
  var unicodeRegEx = /\\u([\d\w]{4})/gi;
  return unicodeString.replace(/\n/g, "<br>").replace(unicodeRegEx, function(match, group) {
    return String.fromCharCode(parseInt(group, 16));
  });
}

editor.keyBinding.addKeyboardHandler({
  handleKeyboard : function(data, hash, keyString, keyCode, event) {
    var editRange = editor.getSelectionRange();
    var editPosition = new Position(editRange.start.row, editRange.start.column);
    if (editPosition.smallerThan(proving) && proved.smallerThan(proving)) {
      return {command:"null", passEvent:false};
    }
  }
});

/**
 * Automatically undo when editing in an area that has been marked as proved.
 * Corresponding positions, markers, and button states are updated.
 */
editor.getSession().on('change', function(delta) {
  var editPosition = new Position(delta.data.range.start.row, delta.data.range.start.column);
  if (proved.row === 0 && proved.column === 0) {
    return;
  }

  if (editPosition.smallerEqual(proved)) {
    proved = findPreviousTactic(editPosition);
    proving = new Position(proved.row, proved.column);
    prover.undo(proved.toGlobalPosition());
    updateMarkers(proved, proving);
    determineActionButtonStates();
  }

  if($('.coq-control-down').hasClass('disabled'))
    determineActionButtonStates();

  changed = true;
});

$('.coq-control-down').click(function(){
    Controls.down();
    editor.focus();
});

$('.coq-control-up').click(function(){
    Controls.up();
    editor.focus();
});

$('.coq-control-all-down').click(function(){
    Controls.allDown();
    editor.focus();
});

$('.coq-control-all-up').click(function(){
    Controls.allUp();
    editor.focus();
});

$('.coq-control-current').click(function(){
    Controls.current();
    editor.focus();
});

$('.coq-control-down, .coq-control-up').click(determineActionButtonStates);

$(".coq-control-login").click(function() {
  $("#course").val("nogroup");
  $("#username").val("");
  $("#password").val("");

  $("#loginModal").modal("show");
});

$(".coq-control-logout").click(function() {
  doLogin(null, null, null, true);

  var parent = $(".coq-control-logout").parent();
  $(".coq-control-logout").addClass("hidden");
  $(".coq-control-login").removeClass("hidden").appendTo(parent);
});

$("#loginModal").on("shown.bs.modal", function() {
  $("#username").focus();
});

$("#loginModal #username").bind("keydown", "return", function(e) {
  $("#loginModal #password").focus();
});

$("#loginModal #password").bind("keydown", "return", function(e) {
  login();
});

$("#loginModal .btn-primary").click(function() {
  login();
});

$("#saveModal").on("shown.bs.modal", function() {
  $("#filename").focus();
});

$("#loadModal").on("shown.bs.modal", function() {
  $("#fileSearch").focus();
});

$("#saveModal .btn-primary").click(function() {
  doSave($("#filename").val());
});

$("#saveModal, #saveModal input").bind("keydown", "return", function(e) {
  $("#saveModal").modal("hide");
  doSave($("#filename").val());
  return false;
});

$("#fileSearch").bind("keyup", function(e) {
  var query = $(this).val();
  $("#files").children().hide();
  $("#files").children("a:contains('" + query + "')").show();
});

$("#fileSearch").bind("keydown", "return", function(e) {
  var names = $("#files a:visible");
  names = $(names.get(0));

  names.each(function() { // Always 0 or 1 names
    $("#loadModal").modal("hide");
    doLoad($(this).find("span[class='name']").text());
  });
});

$('.file a').click(function() {
  var action = $(this).data('action');
  if (action.indexOf("save") === 0) {
    save(action);
  }
  else if (action === "new") {
    loadNew();
  }
  else if (action === "load") {
    load();
  }
});

/**
 * Reads login data from the login popup and executes the actual login procedure.
 */
function login() {
  var course = $("#course").val();
  var user = $("#username").val();
  var pass = $("#password").val();

  if (user.trim().length === 0 || pass.trim().length === 0)
    return;

  doLogin(course, user, pass);
}

/**
 * Invokes our ajax.login function and handles the result. This means that
 * either a login failure notification is displayed, or the login button
 * is replaced with a logout button.
 */
var loginFailureTimeout = null;
function doLogin(/* String */ course, /* String */ user, /* String */ pass, /* Boolean */ anonymousUser) {
  clearTimeout(loginFailureTimeout);
  ajax.login(course, user, pass, function(result) {
    if (result === null) {
      $(".loginFailure").removeClass("hidden");
      loginFailureTimeout = setTimeout(function() {
        $(".loginFailure").addClass("hidden");
      }, 2000);
    }
    else {
      $("#loginModal").modal("hide");
      if (!anonymousUser) {
        $(".coq-control-login").addClass("hidden");
        var parent = $(".coq-control-logout").parent();
        $(".coq-control-logout").removeClass("hidden").appendTo(parent);
        $(".loginSuccess").removeClass("hidden");
        setTimeout(function() {
          $(".loginSuccess").addClass("hidden");
        }, 2000);
      }
    }
  });
}

/**
 * Show the save dialog if the current file has not been saved earlier,
 * or if "Save As" is chosen. In all other cases the editor's contents
 * are saved.
 */
function save(action) {
  if (action === "saveas" || filename === null) {
    $("#filename").val("");
    $("#saveModal").modal("show");
  }
  else {
    doSave();
  }
}

/**
 * Load the list of files and show it to the user. The user can then
 * choose one of the files, upon which the file is loaded.
 */
function load() {
  ajax.loadFileList(function(fileNames) {
    var div = $("#files");
    div.empty();

    fileNames.forEach(function(file) {
      var a = $("<a/>");
      a.attr("href", "#");
      a.addClass("list-group-item file-item");
      a.click(function(){
        $("#loadModal").modal("hide");
        doLoad(file.name);
      });

      var nameSpan = $("<span/>");
      nameSpan.addClass("name");
      nameSpan.text(file.name);
      a.append(nameSpan);

      if (file.status) {
        var statusSpan = $("<span/>");
        statusSpan.addClass("glyphicon pull-left");
        if (file.status !== "Not touched") {
          statusSpan.addClass("glyphicon-" + (file.status === "Solved" ? "ok" :
                                             (file.status === "Correct" ? "flag" : "remove")));
          statusSpan.addClass("text-" + (file.status === "Solved" ? "success" :
                                        (file.status === "Correct" ? "warning" : "danger")));
        }
        statusSpan.attr("title", file.status);
        a.append(statusSpan);
      }

      if (file.reset && file.status !== "Not touched") {
        var resetA = $("<a/>");
        resetA.addClass("glyphicon glyphicon-refresh text-muted");
        resetA.attr("href", "#");
        resetA.attr("title", "reset");
        resetA.click(function() {
          file.name = "Reset " + file.name;
        });
        a.append(resetA);
      }

      if (file.difficulty) {
        var difficultySpan = $("<span/>");
        difficultySpan.addClass("label pull-right");
        difficultySpan.addClass("label-" + (file.difficulty === "Elementary" ? "info" :
                                           (file.difficulty === "Easy" ? "success" : 
                                           (file.difficulty === "Medium" ? "warning" : 
                                           (file.difficulty === "Difficult" ? "danger" : "dead")))));
        difficultySpan.text(file.difficulty);
        a.append(difficultySpan);
      }

      div.append(a);
    });

    $("#fileSearch").val("");
    $("#loadModal").modal("show");
  });
}

/**
 * Loads a new, empty file in the editor.
 */
function loadNew() {
  doLoad(null);
}

/**
 * Save the editor's contents using the specified name. The result is
 * 'parsed' and a success/fail message is shown if appropriate.
 */
var filename = null;
function doSave(/* String */ name) {
  filename = name || filename;
  ajax.post("save", filename + "*" + editor.getSession().getValue(), function(result) {
    $(".saveResult").removeClass("hidden");

    if (result === "++") {
      changed = false;
      $(".saveResult .message").text("Save success!");
      $(".saveResult").addClass("btn-success");
      $(".saveResult .glyphicon").addClass("glyphicon-ok");
      $(document).prop("title", filename + " - " + "ProofWeb");
    }
    else {
      $(".saveResult .message").text("Save failed");
      $(".saveResult").addClass("btn-danger");
      $(".saveResult .glyphicon").addClass("glyphicon-remove");
    }
    setTimeout(function() {
      $(".saveResult").addClass("hidden");
      $(".saveResult").removeClass("btn-success");
      $(".saveResult").removeClass("btn-danger");
      $(".saveResult .glyphicon").addClass("glyphicon-ok");
      $(".saveResult .glyphicon").removeClass("glyphicon-remove");
    }, 2000);
  });
}

/**
 * Actually loads a file. The response of the server is inserted
 * in the editor and the title of the browser tab is updated.
 */
function doLoad(/* String */ name) {
  ajax.load(name, function(content) {
    if (name.indexOf("Reset ") === 0) {
      name = name.substring("Reset ".length);
    }

    proved = new Position(0, 0);
    proving = new Position(0, 0);
    updateMarkers(proved, proving);

    editor.setValue(content);
    editor.getSession().getSelection().setSelectionRange(new Range(0, 0, 0, 0));
    editor.getSession().getUndoManager().reset();
    editor.focus();

    determineActionButtonStates();

    $("#coq-result").html("");
    $("#coq-warnings").html("");
    $("#coq-warnings").addClass("hidden");
    $("#coq-tree .content").html("");

    changed = false;
    filename = name;
    $(document).prop("title", (name !== null ? name + " - " : "") + "ProofWeb");
  });
}

/**
 * Add a handler to the proof-display-button. The new display setting is
 * sent to the prover class, which, if necessary, immediately sends this
 * to the server so that the tree is updated.
 */
$('.displays a').click(function() {
  $('.displays').find("a").removeClass('bg-success');
  $(this).addClass('bg-success');

  prover.setDisplay(Prover.Display[$(this).data('name')]);

  if ($(this).data('name') == 'NONE') {
    $('#coq-tree').hide();
  }
  else {
    $('#coq-tree').show();
    $('#coq-tree').removeClass('hidden');

    prover.display(proved.toGlobalPosition());
  }
});

/**
 * The insert-buttons (backward, forward) contain elements with the value to insert in
 * their 'data' property. We add a handler to every item to insert the values upon click.
*/
$('.inserts a').click(function() {
  Controls.insert($(this).data('insert'));
  editor.focus();
});

/**
 * Toggle menu buttons on hover, not only on click.
 */
$('.btn-group > .btn-group > .btn').mouseenter(function(){$(this).click()});
$('.btn-group > .btn-group').mouseleave(function(){$(this).find('.btn').click(); editor.focus();});

window.onbeforeunload = function(e) {
  if (changed)
    return "There are unsaved changes, are you sure you wish to discard these changes and quit?";
};

/**
 * Focus in de editor and create session on load. We also retrieve the available
 * courses from the server and add them to the login popup (as logging in requires
 * a course). Finally, we log in as an anonymous user.
 */
$(function() {
  editor.focus();
  editor.getSession().setNewLineMode("unix");

  ajax.loadCourseList(function(courses) {
    for (key in courses) {
      $("#course").append("<option value=\"" + key + "\">" + courses[key] + "</option>");
    }
  });

  ajax.login(); // Log the user in as anonymous user

  determineActionButtonStates();
});

$('#open-proof-new-window').click(function(){
  var generator=window.open('','name','height=400,width=500,resizable=yes');
  generator.document.write('<html><head><title>Proof tree</title><style>body{ font-family: monospace }</style></head><body>');
  generator.document.write($("#coq-tree .content").html());
  generator.document.write('</body></html>');
  generator.document.close();
});
