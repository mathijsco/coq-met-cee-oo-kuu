/*
In controls.js the "proof next", "proof previous", "proof all" en "unproof all" buttons are handled.
The getNextTactic-method is a helper to determine where the next tactic is.
The function determineActionButtonStates is used to make buttons idle when they can't do anything.
*/
var Controls = new function() {
  function getNextTactic()
  {
    var prev = new Position(proving.row, proving.column);
    var newProving = findNextTactic(prev); // Prevent overwriting proving with null
    if(newProving == null)
      return null;

    var tactic = getNextTacticValue(prev);
    proving = newProving;
    return new Tactic(Tactic.Type.PROVE, tactic, prev.toGlobalPosition(), proving.toGlobalPosition());
  }

  this.down = function() {
    if($('.coq-control-down').hasClass('disabled'))
      return false;

		var tactic = getNextTactic();
    if(tactic == null)
      return false;

    prover.prove([tactic]);
    updateMarkers(proved, proving);
    determineActionButtonStates();
    return true;
  };

  this.up = function() {
    if($('.coq-control-up').hasClass('disabled'))
      return false;

    if(proved.smallerThan(proving))
      return false; // Prevent undoing while proving

    var oldPos = new Position(proved.row, proved.column);
    proved = findPreviousTactic(proved);
    proving = new Position(proved.row, proved.column);
    prover.undo(proved.toGlobalPosition(), oldPos.toGlobalPosition());
    updateMarkers(proved, proving);
    determineActionButtonStates();
    return true;
  };

  this.current = function() {
    var cPos = editor.selection.getCursor();
    var cursorPosition = new Position(cPos.row, cPos.column);

    if(proving.smallerThan(cursorPosition))
    {
      while(proving.smallerThan(cursorPosition) && !(cursorPosition.row == proving.row && cursorPosition.column == proving.column + 1))
      {
        var result = Controls.down();
        if(!result)
          return;
      }
    }
    else
    {
      while(cursorPosition.smallerThan(proving))
      {
        var result = Controls.up();
        if(!result)
          return;
      }
    }

    determineActionButtonStates();
  };

  this.allDown = function() {
    if($('.coq-control-all-down').hasClass('disabled'))
      return;

    var tactic = getNextTactic();
    if(tactic == null)
      return;
    var tactics = [];

    while(tactic != null)
    {
      tactics.push(tactic);
      tactic = getNextTactic();
    }

    prover.prove(tactics);
    determineActionButtonStates();
  };

  this.allUp = function() {
    if($('.coq-control-all-up').hasClass('disabled'))
      return;

    if(proved.smallerThan(proving))
      return false; // Prevent undoing while proving

    prover.undo(0, proved.toGlobalPosition());
    proved = new Position(0, 0);
    updateMarkers(proved, proving);
    determineActionButtonStates();
  };

  this.insert = function(value) {
    editor.insert(value.replace(/\\n/gi, "\n"));
  };
};

function determineActionButtonStates()
{
  var lines = editor.getSession().getLength();
  var lineLength = editor.getSession().getLine(lines - 1).length - 1;
  if(lines <= proving.row + 1 && lineLength <= proving.column)
  {
    $('.coq-control-down').addClass('disabled');
    $('.coq-control-all-down').addClass('disabled');
  }
  else
  {
    $('.coq-control-down').removeClass('disabled');
    $('.coq-control-all-down').removeClass('disabled');
  }

  if(proving.isZero())
  {
    $('.coq-control-up').addClass('disabled');
    $('.coq-control-all-up').addClass('disabled');
  }
  else
  {
    $('.coq-control-up').removeClass('disabled');
    $('.coq-control-all-up').removeClass('disabled');
  }
}
