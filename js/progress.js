var Progress = new function(){
  var minimumValue = 0;
  var maximumValue = 0;
  var currentValue = 0;
  var reverse = false;
  var timeout = null;
  var DELAY = 500;

  this.setMinimum = function(min){
    if(minimumValue > 0 && min >= minimumValue)
      return;

    minimumValue = min;
  }

  this.setMaximum = function(max){
    if(maximumValue > 0)
    {
      maximumValue = max;
      update();
      return;
    }

    timeout = setTimeout(function() {
      timeout = null;

      $('.progress-bar').removeClass('animated').removeClass('progress-bar-danger');

      if(reverse)
        $('.progress .progress-bar').css('width', '100%');
      else
        $('.progress .progress-bar').css('width', '0%');

      if(!$('.progress').hasClass('visible'))
        $('.progress').addClass('visible');

      $('.progress .progress-bar').addClass('animated').addClass('active');
    }, DELAY);

    maximumValue = max;
  };

  this.setCurrent = function(current){
    if(maximumValue == 0 && minimumValue == 0)
      return;

    currentValue = current;
    update();
  }

  this.setUndo = function(undo){
    reverse = undo;
  }

  this.done = function(current){
    if(maximumValue == 0 && minimumValue == 0)
      return;

    if(current == maximumValue || (reverse && current == minimumValue))
    {
      setCurrent(current);
      return;
    }

    if (timeout !== null) {
      clearTimeout(timeout);
      timeout = null;
    }

    $('.progress-bar').addClass('progress-bar-danger');
    $('.progress-bar').removeClass('active');
    $('.progress').removeClass('visible');

    minimumValue = 0;
    currentValue = 0;
    maximumValue = 0;
  }

  function update()
  {
    var percentage = 0;

    if(!reverse) // Prove
    {
      if(maximumValue - minimumValue != 0 || currentValue - minimumValue != 0)
        percentage = ((currentValue - minimumValue) / (maximumValue - minimumValue)) * 100;

      if(maximumValue == currentValue)
      {
        clearTimeout(timeout);
        timeout = null;

        $('.progress .progress-bar').css('width', '100%');
        $('.progress').removeClass('visible');
        $('.progress-bar').removeClass('active');

        minimumValue = 0;
        currentValue = 0;
        maximumValue = 0;
        return;
      }
    }
    else // Undo
    {
      if(currentValue < minimumValue)
        minimumValue = currentValue; // Because the server might decide to undo further than we anticipate
      if(maximumValue != 0 || currentValue != 0)
        percentage = (((currentValue - minimumValue) / (maximumValue - minimumValue)) * 100);

      if(minimumValue == currentValue)
      {
        clearTimeout(timeout);
        timeout = null;

        $('.progress .progress-bar').css('width', '0%');
        $('.progress').removeClass('visible');
        $('.progress-bar').removeClass('active');

        minimumValue = 0;
        currentValue = 0;
        maximumValue = 0;
        reverse = false;
        return;
      }
    }

    $('.progress .progress-bar').css('width', percentage.toString() + '%');
  }
};
