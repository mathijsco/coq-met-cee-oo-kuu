function Tactic(/* Tactic.Type */ type, /* String */ text, /* Integer */ startIndex, /* Integer */ endIndex) {
  if (type !== Tactic.Type.DISPLAY && type !== Tactic.Type.PROVE && type !== Tactic.Type.UNDO && type !== Tactic.Type.LISTEN) {
    console.error("Tactic type '" + type + "' is not valid");
    return;
  }

  this.type = type;
  this.text = text;
  this.startIndex = startIndex;
  this.endIndex = endIndex;
}

Tactic.Type = {
  DISPLAY: "addtobuf",
  PROVE: "addtobuf",
  UNDO: "undo",
  LISTEN: "listen"
};

// Find the next tactic from the given position.
// Searches for the next dot and returns the index of this dot,
// or null in case no dot was found after the given position.
function findNextTactic(fromPos)
{
  var ti = new TokenIterator(editor.getSession(), fromPos.row, fromPos.column);
  var token = ti.getCurrentToken();

  if(token != null && token.type == "dot")
  {
    ti.stepForward();
    token = ti.getCurrentToken();
  }

  while(token != null)
  {
    if(token.type == "dot")
      return new Position(ti.getCurrentTokenRow(), ti.getCurrentTokenColumn() + 1);

    ti.stepForward();
    token = ti.getCurrentToken();
  }

  if(editor.getSession().getLength() > fromPos.row)
    return findNextTactic(new Position(fromPos.row + 1, 0));

  return null;
}

// Get the actual textual value of the next tactic.
// This function assumes there *is* a next tactic.
function getNextTacticValue(fromPos)
{
  var ti = new TokenIterator(editor.getSession(), fromPos.row, fromPos.column);
  var token = ti.getCurrentToken();
  var tokenValue = '';

  if(token != null && token.type == "dot")
  {
    ti.stepForward();
    token = ti.getCurrentToken();
  }

  while(token != null)
  {
    tokenValue += token.value;

    if(token.type == "dot")
      return tokenValue;

    ti.stepForward();
    token = ti.getCurrentToken();
  }

  return getNextTacticValue(new Position(fromPos.row + 1, 0));
}


// Find the previous tactic from the given position.
// This comes down to 'find the previous dot'.
// The index returned is the index of the dot in case a previous tactic was found,
// or position zero (row 0, column 0) otherwise.
function findPreviousTactic(fromPos)
{
  var ti = new TokenIterator(editor.getSession(), fromPos.row, fromPos.column);
  var token = ti.getCurrentToken();

  while(token != null)
  {
    if(token.type == "dot")
      return new Position(ti.getCurrentTokenRow(), ti.getCurrentTokenColumn());

    ti.stepBackward();
    token = ti.getCurrentToken();
  }

  if(fromPos.row > 0)
    return findPreviousTactic(new Position(fromPos.row - 1, editor.getSession().getLine(fromPos.row - 1).length));

  return new Position(0, 0);
}
