/**
 * Marker class that holds a 'marker' object. A marker consists of three parts.
 * - The first part, consisting of a part of the first row to mark.
 * - The second part, which is a block of n lines which are to be marked completely.
 * - The third part, consisting of a part of the last row to mark.
 * All parts are optional. A marker has a string that can be used to 'classify'
 * the markers in the editor. This classes should be used to style a specific marker.
 */
function Marker(/* String */ classes) {

  // Respectively part two, one, and three of the marker
  var blockId;
  var textId1;
  var textId2;

  /**
   * Calculates and displays a marker from the 'from' position to the 'to' position.
   */
  this.mark = function(/* Position */ from, /* Position */ to) {
    this.remove();

    // Scroll the line to highlight to the visible area
    if(to.row >= editor.getLastVisibleRow() || to.row <= editor.getFirstVisibleRow())
    	editor.scrollToLine(to.row, true, false, function(){});

    var range;

    // Mark part 1 (text1) in case the start and end row are the same
    if (from.row === to.row) {
      if (from.column === to.column)
        return;

      range = new Range(from.row, from.column, to.row, to.column + 1);
      textId1 = editor.getSession().addMarker(range, classes);
      return; // Nothing else to mark
    }
    // Mark part 1 (text1) in case the start and end row are not the same
    else {
      var hasLatterPart = false;
      if (from.column > 0) { // Mark the latter part of the first row, if required
        var fromRowWidth = editor.getSession().getLine(from.row).length;

        hasLatterPart = fromRowWidth > from.column + 1;
        if (hasLatterPart) { // If not, there are no columns left on the from.row
          range = new Range(from.row, from.column + 1, from.row, fromRowWidth);
          textId1 = editor.getSession().addMarker(range, classes);
        }
      }
    }

    // Mark part 2 (the block)
    if (from.row < to.row) { // Check if there is a middle part to mark
      if (hasLatterPart || from.row > 0)
        range = new Range(from.row + 1, 0, to.row - 1, 1);
      else
        range = new Range(from.row, 0, to.row - 1, 1);

      blockId = editor.getSession().addMarker(range, classes, "fullLine");
    }

    // Mark part 3 (text 2)
    range = new Range(to.row, 0, to.row, to.column + 1);
    textId2 = editor.getSession().addMarker(range, classes);
  }

  /**
   * Removes this marker from the editor
   */
  this.remove = function() {
    if (blockId !== -1) {
      editor.getSession().removeMarker(blockId);
      blockId = -1;
    }
    if (textId1 !== -1) {
      editor.getSession().removeMarker(textId1);
      textId1 = -1;
    }
    if (textId2 !== -1) {
      editor.getSession().removeMarker(textId2);
      textId2 = -1;
    }
  }
}

var provedMarker = new Marker('editor-locked proved');
var provingMarker = new Marker('editor-locked proving');

/**
 * Updates the markers. The (green) proved marker is always displayed,
 * whereas the (yellow) proving marker is displayed only if its end
 * position is greater than the proved's end position.
 */
function updateMarkers(/* Position */ proved, /* Position */ proving) {
  provedMarker.mark(Position.ZERO, proved);

  if (!proving.smallerEqual(proved))
    provingMarker.mark(proved, proving);
  else
    provingMarker.remove();
}
