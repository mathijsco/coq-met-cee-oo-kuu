$(function(){
    $('body, #editor textarea').bind("keydown", "ctrl+s", function(e) {
      save();
      return false;
    });

    $('body, #editor textarea').bind("keydown", "meta+s", function(e) {
      save();
      return false;
    });

    $('body, #editor textarea').bind("keydown", "ctrl+shift+s", function(e) {
      save("saveas");
      return false;
    });

    $('body, #editor textarea').bind("keydown", "meta+shift+s", function(e) {
      save("saveas");
      return false;
    });

    $('body, #editor textarea').bind("keydown", "ctrl+o", function(e) {
      load();
      return false;
    });

    $('body, #editor textarea').bind("keydown", "meta+o", function(e) {
      load();
      return false;
    });

    $('body, #editor textarea').bind("keydown", "ctrl+m", function(e) {
      loadNew();
      return false;
    });

    $('body, #editor textarea').bind("keydown", "meta+m", function(e) {
      loadNew();
      return false;
    });

    // For windows and linux
    $('body, #editor textarea').bind('keydown', 'ctrl+down', function(e){
        Controls.down();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'ctrl+up', function(){
        Controls.up();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'ctrl+right', function(){
        Controls.current();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'ctrl+return', function(){
      Controls.current();
      return false;
    });

    $('body, #editor textarea').bind('keydown', 'ctrl+shift+down', function(e){
        Controls.allDown();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'ctrl+shift+up', function(){
        Controls.allUp();
        return false;
    });
    ////////////////////////////////////////////////////////////////////////////
    // For Mac OS X
    $('body, #editor textarea').bind('keydown', 'meta+down', function(e){
        Controls.down();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'meta+up', function(){
        Controls.up();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'meta+right', function(){
        Controls.current();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'meta+return', function(){
      Controls.current();
      return false;
    });

    $('body, #editor textarea').bind('keydown', 'alt+meta+down', function(e){
        Controls.allDown();
        return false;
    });

    $('body, #editor textarea').bind('keydown', 'alt+meta+up', function(){
        Controls.allUp();
        return false;
    });

    $("#editor textarea").bind('keyup', '.', function(){
    	if ($('#settings-electric').hasClass('btn-primary'))
        	Controls.current();

    	return true;
    });

    $("#editor textarea").bind('keyup', function(){
    	determineActionButtonStates();
    });
});

$(function(){
  if(navigator.platform.indexOf('Mac') == -1)
  {
    $('.coq-control-up').each(function(){$(this).attr('title', $(this).attr('title') + ' (Ctrl+↑)')});
    $('.coq-control-down').each(function(){$(this).attr('title', $(this).attr('title') + ' (Ctrl+↓)')});
    $('.coq-control-current').each(function(){$(this).attr('title', $(this).attr('title') + ' (Ctrl+→)')});
    $('.coq-control-all-up').each(function(){$(this).attr('title', $(this).attr('title') + ' (Ctrl+Shift+↑)')});
    $('.coq-control-all-down').each(function(){$(this).attr('title', $(this).attr('title') + ' (Ctrl+Shift+↓)')});
    $('#new-button').attr('title', 'Ctrl+M');
    $('#load-button').attr('title', 'Ctrl+O');
    $('#save-button').attr('title', 'Ctrl+S');
    $('#saveas-button').attr('title', 'Ctrl+Shift+S');
    editor.commands.bindKeys({"ctrl-down":null, "ctrl-up":null, "ctrl-right":null, "ctrl-shift-down":null, "ctrl-shift-up":null, "ctrl-t":null});
  }
  else
  {
    $('.coq-control-up').each(function(){$(this).attr('title', $(this).attr('title') + ' (⌘↑)')});
    $('.coq-control-down').each(function(){$(this).attr('title', $(this).attr('title') + ' (⌘↓)')});
    $('.coq-control-current').each(function(){$(this).attr('title', $(this).attr('title') + ' (⌘→)')});
    $('.coq-control-all-up').each(function(){$(this).attr('title', $(this).attr('title') + ' (⌥⌘↑)')});
    $('.coq-control-all-down').each(function(){$(this).attr('title', $(this).attr('title') + ' (⌥⌘↓)')});
    $('#new-button').attr('title', '⌘M');
    $('#load-button').attr('title', '⌘O');
    $('#save-button').attr('title', '⌘S');
    $('#saveas-button').attr('title', '⌘⇧S');
    editor.commands.bindKeys({"meta-down":null, "meta-up":null, "alt-meta-down":null, "alt-meta-up":null, "meta-r":null});
  }
});
