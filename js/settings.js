/* Settings management for the editor.
 * Currently implemented:
 * Wordwrap on/[off]
 * Electric terminator on/[off]
 * Font size [9]
 * Editor width [2/3 of window width]
 */
var $settingWordwrapOn = '<span class="glyphicon glyphicon-ok"></span> Enabled';
var $settingWordwrapOff = '<span class="glyphicon glyphicon-remove"></span> Disabled';
var $settingElectricOn = '<span class="glyphicon glyphicon-flash"></span> Enabled';
var $settingElectricOff = '<span class="glyphicon glyphicon-remove"></span> Disabled';

// Listeners for the buttons in the settings screen
$('#settings-fontsize').change(function(){
  $('#editor').css('font-size', $(this).val() + 'pt');
  setSetting("editor-fontsize", $(this).val());
});

$('#settings-wordwrap').click(function(e){
  e.preventDefault();
  editor.getSession().setUseWrapMode(!editor.getSession().getUseWrapMode());
  setSetting("editor-wordwrap", (editor.getSession().getUseWrapMode() ? 1 : 0));
  toggleWrapButton();
});

$('#settings-electric').click(function(e){
  e.preventDefault();
  toggleElectricButton();
  setSetting("editor-electric", ($('#settings-electric').hasClass('btn-primary') ? 1 : 0));
});


function toggleWrapButton()
{
  if(editor.getSession().getUseWrapMode())
    $('#settings-wordwrap').removeClass('btn-default').addClass('btn-primary').html($settingWordwrapOn);
  else
    $('#settings-wordwrap').removeClass('btn-primary').addClass('btn-default').html($settingWordwrapOff);
}

function toggleElectricButton()
{
  if($('#settings-electric').hasClass('btn-default'))
    $('#settings-electric').removeClass('btn-default').addClass('btn-primary').html($settingElectricOn);
  else
    $('#settings-electric').removeClass('btn-primary').addClass('btn-default').html($settingElectricOff);
}

function getCookie(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
  }
  return false;
}

function getSetting(name, defaultValue)
{
  var value = getCookie(name);
  if(value)
    return value;

  setSetting(name, defaultValue);
  return defaultValue;
}

function setSetting(name, value)
{
  document.cookie = name + "=" + value;
}

// Make editor resizeable
$(".resizable-e").resizable({ handles: "e" }).on("resizestop", function(event, ui){
  editor.resize();
}).on("resize", function(event, ui){
  $(".sidediv").css("left", ui.size.width.toString() + "px");
  setSetting('editor-width', ui.size.width.toString());
});


// Load settings or default values on initialization
$(function(){
  var fontsize = getSetting('editor-fontsize', 9);
  $('#editor').css('font-size', fontsize.toString() + 'pt');
  $('#settings-fontsize').val(fontsize);

  var wordwrap = getSetting('editor-wordwrap', 0);
  if(wordwrap == 1)
    editor.getSession().setUseWrapMode(true);
  else
    editor.getSession().setUseWrapMode(false);

  toggleWrapButton();
  toggleElectricButton();

  var electric = getSetting('editor-electric', 0);
  if(electric == 1)
    toggleElectricButton();

  var width = getSetting('editor-width', ($(window).width() / 3) * 2);
  $(".resizable-e").css("width", width.toString() + "px")
  $(".sidediv").css("left", width.toString() + "px");
  editor.resize();

  // Verwijder de print-margin indicator
  $('.ace_print-margin').css('visibility', 'hidden');
});
