/**
 * This utility 'class' provides a set of methods that allows you to communicate
 * with the server. You can login, load files, save files, and execute all Coq
 * related commands.
 */
var ajax = new function() {

  var PROVER = "coq";
  var DEFAULT_COURSE = "nogroup";
  var DEFAULT_USER = "nobody";
  var DEFAULT_PASS = "anon";

  // var HOST = window.location.origin; // Use this if the backend runs on the same server as the frontend
  var HOST = "http://pwdev.cs.ru.nl"; // Otherwise, hard-code it here
  var PATH = "/pub";
  var INDEX = "/index.html";
  var LOGIN = "/login.php";
  var LOGGED = "/logged.html";

  var sequence = 0;
  var session = null; // This is set on load
  var currentUser = null;
  var currentPass = null;

  //

  /**
   * Send a login command the the server. Unfortunately, the server has no such
   * command, so instead we load a 'null' file with the specified credentials,
   * or the default (anonymous) credentials. Loading a file creates a session.
   * We save this session and are happy. The callback is called when the server
   * responded to our request.
   */
  this.login = function(/* String */ course, /* String */ user, /* String */ pass, /* funtion(String result) */ callback) {
    course = course || DEFAULT_COURSE;
    currentUser = course + "/" + (user || DEFAULT_USER);
    currentPass = pass || DEFAULT_PASS;

    this.load(null, callback);
  };

  /**
   * Loads the courses page, extracts all course names, and creates a map
   * from course id to course name. This map is fed to the callback function.
   */
  this.loadCourseList = function(/* function(Map<String, String> courses) */ callback) {
    $.post(HOST + LOGIN, function(html) {
      var div = $("<div/>").html(html);
      var select = div.find("select[name='course']");
      var nodes = select.find("option");
      var names = new Object();
      nodes.each(function (i) {
        var id = $(this).attr("value") || "nogroup";
        var name = $(this).text() || "None";

        names[id] = name;
      });

      callback(names);
    });
  };

  /**
   * Loads the file overview page for the current user. This page is parsed,
   * the file names are extracted, and an array of these names is given to
   * the callback function. This function receives an array of objects, and
   * each object contains 'name', and if available also 'status', 'difficulty',
   * and 'reset'. All of 'name', 'difficulty', and 'status' are strings; and
   * 'reset' is a boolean, indicating whether resetting this file is possible.
   */
  this.loadFileList = function(/* function(Object[] fileNames) */ callback) {
    $.post(HOST + LOGGED, {
      prover: PROVER,
      login: currentUser,
      pass: currentPass
    }, function(html) {
      var div = $("<div/>").html(html);
      
      var names = new Array();
      if (currentUser === DEFAULT_COURSE + "/" + DEFAULT_USER) {
        var nodes = div.find("input[name='cmdarguments']");
        nodes.each(function() {
          names.push({ name: $(this).attr("value") });
        });
      }
      else {
        var nodes = div.find("tr");
        nodes.each(function() {
          var nameNode = $(this).find("input[name='cmdarguments']:not('class')");
          if (nameNode.length === 0)
            return;
          
          var difficultyNode = $(this).find("td:nth-child(2)");
          var statusNode = $(this).find("font");
          
          names.push({
            name: nameNode.attr("value"),
            difficulty: difficultyNode.text(),
            status: statusNode.text(),
            reset: true
          });
        });
      }

      if (typeof callback === "function") {
        callback(names);
      }
    });
  };

  /**
   * Loads the specified file from the server. The server returns a HTML page, which
   * we parse and extract the file contents from. The result is fed to the callback.
   * Note that calling that method gives us a new session, due to the way the server
   * works.
   */
  this.load = function(/* String */ fileName, /* function(initialInput) */ callback) {
    fileName = fileName || "";
    $.post(HOST + INDEX, {
      prover: PROVER,
      login: currentUser,
      pass: currentPass,
      adminlogin: "",
      adminpass: "",
      cmdarguments: fileName
    }, function(html) {
      var content = null;

      if (html.indexOf("session") !== -1) { // Else the login details are wrong
        var sessionString = html.split("session=\"")[1];
        session = sessionString.substring(0, sessionString.indexOf("\""));

        var contentString = html.split("hidetext")[1];
        if (contentString !== undefined) {
          content = contentString.substring(contentString.indexOf(">") + 1, contentString.indexOf("</div>")).replace(/<br>/g, "\n");
          content = $("<div/>").html(content).text();
        }
      }

      if (typeof callback === "function") {
        callback(content);
      }
    });
  };

  /**
   * Sends the specified command to the server. An object is created containing this
   * command and corresponding arguments, and the callback is invoked with the raw
   * response data. The valid commands are the tactic types (see tactic.js) and "save".
   */
  this.post = function(/* String */ command, /* String */ args, /* function(data) */ callback) {
    var options = {
      command: command,
      callnr: sequence++,
      s: session,
      cmdarguments: args
    };

    // I hate to do this
    if (command === "save") {
      options.prover = PROVER;
      options.login = currentUser;
      options.pass = currentPass;
    }

    $.post(HOST + PATH + INDEX, options, callback);
  };
}
